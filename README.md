# README #



### What is this repository for? ###

This is a repository that you can use to reproduce the results from the paper "Neemt de frequentie van sfeerwoorden in tweets toe zodra het Kerst
wordt?".

### How do I get set up? ###

* You first need to log in on the server of the Rijksuniversiteit Groningen. You can do that by accessing a UWP at the university or log in locally with a ssh.
* You need to fork this repository to your home, and run the shellscript.