#!/bin/bash

# 19-12 (Time before Christmas)
echo "19-12 (Time before Christmas)"
zless /net/corpora/twitter2/Tweets/2016/12/20161219\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 20-12 (Time before Christmas)
echo "20-12 (Time before Christmas)"
zless /net/corpora/twitter2/Tweets/2016/12/20161220\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 21-12 (Time before Christmas)
echo "21-12 (Time before Christmas)"
zless /net/corpora/twitter2/Tweets/2016/12/20161221\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 22-12 (Time before Christmas)
echo "22-12 (Time before Christmas)"
zless /net/corpora/twitter2/Tweets/2016/12/20161222\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 23-12 (Time before Christmas)
echo "23-12 (Time before Christmas)"
zless /net/corpora/twitter2/Tweets/2016/12/20161223\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 24-12 (Time before Christmas)
echo "24-12 (Time before Christmas)"
zless /net/corpora/twitter2/Tweets/2016/12/20161224\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 25-12 (First Christmas Day)
echo "25-12 (First Christmas Day)"
zless /net/corpora/twitter2/Tweets/2016/12/20161225\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 26-12 (Second Christmas Day)
echo "26-12 (Second Christmas Day)"
zless /net/corpora/twitter2/Tweets/2016/12/20161226\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
