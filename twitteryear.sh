#!/bin/bash

# 15-01 (Random date (not a holiday))
echo "15-01 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/01/20160115\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 23-02 (Random date (not a holiday))
echo "23-02 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/02/20160223\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 15-03 (Random date (not a holiday))
echo "15-03 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/03/20160315\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 10-04 (Random date (not a holiday))
echo "10-04 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/04/20160410\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 15-05 (Random date (not a holiday))
echo "15-05 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/05/20160515\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 12-06 (Random date (not a holiday))
echo "12-06 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/06/20160612\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 20-07 (Random date (not a holiday))
echo "20-07 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/07/20160720\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 22-08 (Random date (not a holiday))
echo "22-08 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/08/20160822\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 10-09 (Random date (not a holiday))
echo "10-09 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/09/20160910\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 12-10 (Random date (not a holiday))
echo "12-10 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/10/20161012\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 20-11 (Random date (not a holiday))
echo "20-11 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/11/20161120\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
# 01-12 (Random date (not a holiday))
echo "01-12 (Random date (not a holiday))"
zless /net/corpora/twitter2/Tweets/2016/12/20161201\:12.out.gz | /net/corpora/twitter2/tools/tweet2tab -i text | sort | uniq | grep -w -e 'gezellig' -e 'blij' -e 'familie' -e 'liefde' -e 'tevreden' -e 'vrolijk' -e 'zalig' -e 'gezondheid' -e 'geluk' -e 'dankbaar' -e 'liefdevol' -e 'feest' -e 'vreugde' | wc -l
